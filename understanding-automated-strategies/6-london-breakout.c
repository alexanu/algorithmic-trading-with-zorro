/* LONDON BREAKOUT STRATEGY
Trade brekaouts of fractal within the London session
*/
function run()
{
    set(PLOTNOW);
    setf(PlotMode, PL_FINE);
    StartDate = 20100101;
    EndDate = 20181231;
    BarPeriod = 10;
    LookBack = 100;
    asset("EUR/USD");
    MaxLong = MaxShort = 1;
	
    Spread = Commission = Slippage = RollLong = RollShort = 0;
    
    vars Price = series(price());
    vars Highs = series(priceHigh());
    vars Lows = series(priceLow());
    
    vars trendFilter = series(LowPass(Price, 100));
    
    //short term highs and lows
    int fracTime = 7;
    var fHigh = FractalHigh(Highs, fracTime);
    var fLow = FractalLow(Lows, fracTime);
    static var fHighCurrent, fLowCurrent;
    static var longEntry, shortEntry;
    if(fHigh != 0) fHighCurrent = fHigh;
    if(fLow != 0) fLowCurrent = fLow;
    
    if(fHigh != 0 or fLow != 0)
    {
        shortEntry = Lows[0];
        longEntry = Highs[0];
    }
	
    // trade logic
    EntryTime = 3;
    Stop = 4*ATR(40); 
    ExitTime = 4; 
    Trail = Stop;
    
    if(lhour(WET) >= 8 and lhour(WET) <= 17 and between(priceClose(0), fLowCurrent, fHighCurrent))
    {
        if (Price[0] > trendFilter[0])
        {
            Entry = longEntry;
            enterLong();
        }
        if (Price[0] < trendFilter[0])
        {
            Entry = shortEntry;
            enterShort();   
        }         
    }
    plot("Recent High", fHighCurrent, MAIN|DOT, BLUE);
    plot("Recent Low", fLowCurrent, MAIN|DOT, RED);
    plot("LowPass", trendFilter, MAIN, GREEN);
}