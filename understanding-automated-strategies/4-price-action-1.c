/*
PIN CANDLE STRATEGY
*/

 

function run()
{
	StartDate= 20100101;
	EndDate = 20181231;
	set(PARAMETERS|PLOTNOW);
	setf(PlotMode, PL_FINE);
	MaxLong = MaxShort = 1;
	
	vars Open = series(priceOpen());
	vars Close = series(priceClose());
	vars High = series(priceHigh());
	vars Low = series(priceLow());

	vars range = series(High[0]-Low[0]);
	var closeSegment = optimize(20, 15, 40, 5)/100; //close in the closeSegment percent upper or lower portion
	
	ExitTime = 5;
	
	if(Close[0] > Open[0]) // up bar
	{
		if(Open[0] > High[0]-closeSegment*range[0]) // bullish pin bar
		{
			enterLong();
			plot("bullishPin", High[0] + 5*PIP, MAIN|TRIANGLE, BLUE);		
		}
	}
	
	if(Close[0] < Open[0]) // down bar
	{
		if(Open[0] < Low[0]+closeSegment*range[0]) // bearish pin bar
		{
			enterShort();
			plot("bearishPin", Low[0] - 5*PIP, MAIN|TRIANGLE4, MAGENTA);		
			
		}
	}	
}