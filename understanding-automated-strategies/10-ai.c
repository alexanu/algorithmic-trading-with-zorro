/*
DECISION TREE STRATEGY
*/

function run()
{
	//no in-sample/out-of-sample split. Use walk-forward only
	StartDate= 20110101;
	EndDate = 20181231;
	LookBack = 500;
	BarPeriod = 240;
	TradesPerBar = 2;
  	if(Train) Hedge = 2;
  	set(RULES|PLOTNOW);
	setf(PlotMode, PL_FINE);
  	NumWFOCycles = 20;
	MaxLong = MaxShort = 1;
  
// generate price series
	vars Price = series(price());
  	vars High = series(priceHigh());
  	vars Low  = series(priceLow());
  	vars Close = series(priceClose());

// generate some signals
 	var Signal1 = Normalize(series(LowPass(Price,250)), 50);
  	var Signal2 = Normalize(series(HighPass(Price, 50)), 50);
  	var Signal3 = Normalize(series(High[0]-Low[0]), 100);
  	var Signal4 = Normalize(series(RSI(Price, 9)), 25);
  	var Signal5 = Normalize(series(ADX(20)), 250);

// train signals and test
  	Stop = 1*ATR(100); 
  	TakeProfit = 3*ATR(100);
  	if(adviseLong(DTREE,0,Signal1,Signal2,Signal3,Signal4,Signal5) > 0)
    	enterLong();
  	if(adviseShort(DTREE,0,Signal1,Signal2,Signal3,Signal4,Signal5) > 0)
    	enterShort();

}