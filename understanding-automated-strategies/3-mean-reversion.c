/*
BOLLINGER BAND MEAN REVERSION STRATEGY


*/

function run()
{
	set(PLOTNOW);
	StartDate = 20170101;
	EndDate = 20181231;
	BarPeriod = 120;
	MaxLong = MaxShort = 1;
	setf(PlotMode, PL_FINE);
	asset("SPX500");
	
	vars Close = series(priceClose());
	
	int bbPeriod = 40;
	var bbSD = 2.5;
	BBands(Close, bbPeriod, bbSD, bbSD, MAType_EMA);
	
	Stop = 2*ATR(25);
	TakeProfit = rRealMiddleBand; // exit at middle band value at trade entry
	
	if(crossOver(Close, rRealUpperBand)) 
	{
		enterShort();
	}
	
	if(crossUnder(Close, rRealLowerBand)) 
	{
		enterLong();
	}
	
	plot("upperBB", rRealUpperBand, BAND1, YELLOW);
	plot("lowerBB", rRealLowerBand, BAND2, YELLOW+TRANSP);
	plot("middleBB", rRealMiddleBand, MAIN, BLUE);
		
}