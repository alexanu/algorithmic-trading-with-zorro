function main() 
{ 
	var *myPointer; 
	var myNumber = 503; 
	printf("\nValue assigned to myNumber: %.0f", myNumber); 
	myPointer = &myNumber; 
	//myPointer now contains the memory address of myNumber 
	
	*myPointer = 2015; 
	//*myPointer changes the value myNumber 
	
	printf("\nValue of myNumber after being modified by pointer: %.0f", myNumber); 
}