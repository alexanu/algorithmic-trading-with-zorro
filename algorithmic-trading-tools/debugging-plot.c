function run()
{
    BarPeriod = 1440;
    StartDate = 2015;
    vars Price = series(price());
    var LP30 = LowPass(Price,30);
    var LP100 = LowPass(Price,100);
    var Range = priceHigh(0) - priceLow(0);
    PlotScale = 8; // bigger symbols
    PlotWidth = 1000;
    PlotHeight1 = 300;
    set(PLOTNOW);
    plot("LP30",LP30,0,RED);
    plot("LP100",LP100,0,BLUE);
    plot("Range",Range,NEW|BARS,BLACK);
    // plot 20-period highest highs and lowest lows
    if(HH(20) == priceHigh(0))
        plot("HH20", priceHigh(0)+10*PIP, MAIN|DOT, GREEN);
    if(LL(20) == priceLow(0))
        plot("LL20", priceLow(0)-10*PIP, MAIN|CROSS, ORANGE); 
}