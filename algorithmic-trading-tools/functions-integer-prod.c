// multiply two integers and return the result 
int integer_prod( int x, int y )
{
    int prod;
    prod = x * y;
    printf("\nThe product of x and y is: \%d", prod);
    return prod;
}

function main()
{
    int revenue = 13; //gross revenue per sale
    int numSales = 2467; //total number of sales
    int grossRevenue = integer_prod(revenue, numSales);
    printf("\nGross revenue is: \%d", grossRevenue);
}