void holiday( int amount )
{
    printf("\nSavings: %d", amount);
    if (amount < 10000) printf("\nGo back to work!");
    else printf("\nTake a holiday!");
}

function main()
{
    int savings = 5000;
    holiday(savings);
}