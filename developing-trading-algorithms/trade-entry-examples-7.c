function run()
{
	LookBack = 0; //start trading immediately
	
	Entry = 50*PIP; //pending stop order
	Stop = 25*PIP; //initial stop loss
	Trail = 40*PIP; //trail the maximum profit by 65 pips
	TrailSpeed = 200; //raise the stop loss at twice speed prior to break-even
	
	enterLong(); //enter a pending buy stop order
	
	if(Bar > 5)
		quit(); // quit after five bars
	
}