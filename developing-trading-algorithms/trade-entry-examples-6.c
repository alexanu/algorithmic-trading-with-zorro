function run()
{
	LookBack = 0; //start trading immediately
	
	Entry = 50*PIP; //pending stop order
	Stop = 25*PIP; //initial stop loss
	Trail = 40*PIP; //trail the maximum profit by 65 pips
	TrailStep = 10; //every bar, step the stop loss by 10\% of the distance from max favorable excursion//to current stop loss
	
	enterLong(); //enter a pending buy stop order
	
	if(Bar > 5)
		quit(); // quit after five bars
	
}