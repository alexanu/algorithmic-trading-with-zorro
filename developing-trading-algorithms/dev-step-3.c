/*
REVERSE RSI SYSTEM
* Go long at market when the RSI crosses the overbought threshold
* Go short at market when the RSI crosses the oversold threshold
*/
function run()
{
	//out of sample data
	//StartDate = 20170101;
	//EndDate = 20171231;
    
	//in-sample data
	StartDate = 20120101;
	EndDate = 20161231;
	
	BarPeriod = 60;
	
	//limit positions to maximum 1 long or short at a time 
	MaxLong = MaxShort = 1;
	
	vars Price = series(price()); //mean price
	
	//set up RSI and overbought/oversold levels
	int rsiPeriod = 9;
	vars rsi = series(RSI(Price, rsiPeriod));
	int overbought = 70;
	int oversold = 30; 
	
	//exit parameters
	Stop = 3*ATR(24);
	Trail = 2*Stop;
	
	//entry conditions
	if(crossOver(rsi, overbought))
	{
		enterLong();
	}
	if(crossUnder(rsi, oversold))
	{
		enterShort();
	}
	
	//plots
	PlotBars = 250; //plot first 250 bars only for better viewing
	PlotHeight1 = 400;
	PlotHeight2 = 125;
	PlotWidth = 1200;
	ColorEquity = ColorDD = 0;
	plot("rsi", rsi, NEW, BLUE);
	plot("overbought", overbought, 0, BLACK);
	plot("oversold", oversold, 0, BLACK);
}
