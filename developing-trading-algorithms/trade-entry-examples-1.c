function run()
{
	LookBack = 0; //start trading immediately
	
	Entry = 50*PIP; //pending stop entry distance
	Stop = 25*PIP; //initial stop loss
	TakeProfit = 250*PIP; //take profit target
	
	enterLong(); //enter a pending buy stop order
	
	Entry = -50*PIP; //pending limit entry distance
	
	enterLong(); //enter a pending buy limit order
	
	if(Bar > 5)
		quit(); // quit after five bars
}